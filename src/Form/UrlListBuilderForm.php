<?php

namespace Drupal\hablame\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The form to return to the url shortener form.
 */
class UrlListBuilderForm extends FormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_url_list_builder_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $description = $this->t('Press the button to go back to the test url form.');
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go back'),
    ];
    $form['description'] = [
      '#markup' => '<div>' . $description . '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('hablame.url_short_test');
  }

}
