<?php

namespace Drupal\hablame\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The form to return to the sms test form.
 */
class SmsListBuilderForm extends FormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_sms_list_builder_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $description = $this->t('Press the button to go back to the test sms form.');
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go back'),
    ];
    $form['description'] = [
      '#markup' => '<div>' . $description . '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('hablame.sms_test');
  }

}
