<?php

namespace Drupal\hablame\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\hablame\Service\HablameService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines wkhtmltopdf form configuration.
 */
class SettingsConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'hablame.settings';

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Variable that store the store service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $store;

  /**
   * Class constructor.
   *
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $store
   *   The store service.
   */
  public function __construct(
    HablameService $hablame,
    PrivateTempStoreFactory $store
  ) {
    $this->hablame = $hablame;
    $this->store = $store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hablame.service'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hablame.settings');
    $form['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('ID gotten from Hablame.co'),
      '#required' => 'TRUE',
      '#default_value' => $config->get('account'),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret (API Key)'),
      '#description' => $this->t('API Key string gotten from Hablame.co'),
      '#required' => 'TRUE',
      '#default_value' => $config->get('api_key'),
    ];
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token (Gotten via email)'),
      '#description' => $this->t('token to generate and validate'),
      '#required' => 'TRUE',
      '#default_value' => $config->get('token'),
    ];
    $form['minimum_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum amount'),
      '#description' => $this->t('minimum amount required for the alert system'),
      '#required' => 'TRUE',
      '#default_value' => $config->get('minimum_amount'),
    ];
    $form['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test mode'),
      '#description' => $this->t('Must use test URL paths?'),
      '#default_value' => $config->get('test'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('hablame.settings')
      ->set('account', $form_state->getValue('account'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('token', $form_state->getValue('token'))
      ->set('minimum_amount', $form_state->getValue('minimum_amount'))
      ->set('test', boolval($form_state->getValue('test')))
      ->save();

    $store = $this->store->get('hablame');
    $storeId = 'HablameAccountStatus';
    $result = $this->hablame->getAccountStatus();
    if ($result !== FALSE) {
      $store->set($storeId, $result);
    }
    else {
      $store->delete($storeId);
      $this->messenger()->addError($this->t('Error while checking the account status see the log for more information.'));
    }

    parent::submitForm($form, $form_state);
  }
}
