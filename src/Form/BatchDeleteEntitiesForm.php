<?php

namespace Drupal\hablame\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\hablame\Service\HablameHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form Bynder images export.
 */
class BatchDeleteEntitiesForm extends ConfigFormBase {

  /**
   * The service for processing batch package.
   *
   * @var \Drupal\delete_batch\Service\DataService
   */
  protected $dataService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Variable that store the hablame Helper.
   *
   * @var \Drupal\hablame\Service\HablameHelper
   */
  protected $hablameHelper;

  /**
   * Constructs a new form object.
   *
   * @param \Drupal\hablame\Service\HablameHelper $hablameHelper
   *   The hablame helper.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   */
  public function __construct(
    HablameHelper $hablameHelper,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger
  ) {
    $this->hablameHelper = $hablameHelper;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hablame.helper'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_entities_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['delete_entities_batch_form.sync'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['type_of_entity'] = [
      '#type' => 'select',
      '#title' => 'Select the type of message',
      '#default_value' => 'priority',
      '#options' => [
        'sms' => 'SMS',
        'url_short' => 'Url',
      ],
      '#description' => $this->t('Select the type of SMS to send.'),
    ];

    $form['description'] = [
      '#markup' => $this->t('<div>Press to clean the entities.</div>'),
    ];

    $form['fill'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start'),
      '#weight' => 5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $batchBuilder = (new BatchBuilder())
      ->setTitle($this->t('Entities deleting'))
      ->setFinishCallback([$this->hablameHelper, 'finish']);

    $entityType = $form_state->getValue('type_of_entity');

    if ($entityType === 'sms') {
      $entity = $this->entityTypeManager->getStorage('sms');
    }
    else {
      $entity = $this->entityTypeManager->getStorage('url_short');
    }

    $query = $entity->getQuery();
    $entityIds = $query->execute();

    if (!empty($entityIds)) {
      foreach ($entityIds as $entityId) {
        $batchBuilder->addOperation([
          $this->hablameHelper,
          'processEntityGroup',
        ], [$entityId, $entityType]);
      }
    }
    else {
      $this->messenger->addMessage($this->t('The entities are empty.'));
    }
    batch_set($batchBuilder->toArray());
  }

}
