<?php

namespace Drupal\hablame\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form to know the account status.
 */
class AccountStatusForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Variable that store the module handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Variable that store the store service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $store;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $store
   *   The store service.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    HablameService $hablame,
    PrivateTempStoreFactory $store
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->hablame = $hablame;
    $this->store = $store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('hablame.service'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_account_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hablame.settings');
    $minimum_amount = $config->get('minimum_amount');
    $storeId = 'HablameAccountStatus';
    $module_path = $this->moduleHandler->getModule('hablame')->getPath();
    $ymlFormFields = Yaml::decode(file_get_contents($module_path . '/assets/yml/form/hablame.accountstatus.form.yml'));
    foreach ($ymlFormFields as $key => $field) {
      $form[$key] = $field;
    }

    $store = $this->store->get('hablame');
    $data = $store->get($storeId);
    if (!empty($data)) {
      $result = $data['data'];
      foreach ($result as $key => $value) {
        if (str_contains($key, 'enabled')) {
          $state = $value ? 'Enabled' : 'Disabled';
          $form['information']['account_services_enabled'][$key]['#markup'] = $state;
        }
        else {
          if ($key == "balance") {
            $value_format = $value != 0 ? number_format($value) : $value;
            $form['information']['account_data'][$key]['#markup'] = "$$value_format";
          }
          else {
            $form['information']['account_data'][$key]['#markup'] = $value;
          }
        }

        if ($key == "balance" && $value < $minimum_amount) {
          $form['information']['account_data']['balance']['#prefix'] = '<div class="error">';
          $form['information']['account_data']['balance']['#suffix'] = '</div>';

          $form['information']['account_data']['comment'] = [
            '#markup' => $this->t('Your balance is about to run out,
            go to the following link to add balance.
            (<a target="_blanck" href="https://ecare.hablame.co/finanzas/">ecare.hablame.co</a>)'),
          ];
          $this->messenger()->addError($this->t('Your balance is about to run out,
            go to the following link to add balance'));
        }
      }
    }
    else {
      $result = $this->hablame->getAccountStatus();
      if ($result !== FALSE) {
        $store->set($storeId, $result);
        foreach ($result['data'] as $key => $value) {
          if (str_contains($key, 'enabled')) {
            $state = $value ? 'Enabled' : 'Disabled';
            $form['information']['account_services_enabled'][$key]['#markup'] = $state;
          }
          else {
            if ($key == "balance") {
              $value_format = $value != 0 ? number_format($value) : $value;
              $form['information']['account_data'][$key]['#markup'] = "$$value_format";
            }
            else {
              $form['information']['account_data'][$key]['#markup'] = $value;
            }
          }
          if ($key == "balance" && $value < $minimum_amount) {
            $form['information']['account_data']['balance']['#prefix'] = '<div class="error">';
            $form['information']['account_data']['balance']['#suffix'] = '</div>';
            $form['information']['account_data']['comment'] = [
              '#markup' => $this->t('Your balance is about to run out,
              go to the following link to add balance.
              (<a target="_blanck" href="https://ecare.hablame.co/finanzas/">ecare.hablame.co</a>)'),
            ];
            $this->messenger()->addError($this->t('Your balance is about to run out,
            go to the following link to add balance'));
          }
        }
      }
      else {
        $this->messenger()->addError($this->t('Error while checking the account status see the log for more information.'));
      }
    }

    $form['#attached']['library'][] = 'hablame/styles';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storeId = 'HablameAccountStatus';
    $store = $this->store->get('hablame');

    if (!$store->get($storeId)) {
      $result = $this->hablame->getAccountStatus();
      if ($result !== FALSE) {
        $store->set($storeId, $result);
      }
      else {
        $this->messenger()->addMessage($this->t('Error while checking the account status see the log for more information.'));
      }
    }
    else {
      $store->delete($storeId);
      $result = $this->hablame->getAccountStatus();
      if ($result !== FALSE) {
        $store->set($storeId, $result);
      }
      else {
        $this->messenger()->addMessage($this->t('Error while checking the account status see the log for more information.'));
      }
    }
  }
}
