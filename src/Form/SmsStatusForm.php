<?php

namespace Drupal\hablame\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameHelper;
use Drupal\hablame\Service\HablameService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure hablame settings for this site.
 */
class SmsStatusForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Variable that store the hablame Helper.
   *
   * @var \Drupal\hablame\Service\HablameHelper
   */
  protected $hablameHelper;

  /**
   * Variable that store the module handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\hablame\Service\HablameHelper $hablameHelper
   *   The hablame helper.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    HablameService $hablame,
    HablameHelper $hablameHelper
    ) {
    $this->moduleHandler = $moduleHandler;
    $this->hablame = $hablame;
    $this->hablameHelper = $hablameHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('hablame.service'),
      $container->get('hablame.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_sms_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $module_path = $this->moduleHandler->getModule('hablame')->getPath();

    $ymlFormFields = Yaml::decode(file_get_contents($module_path . '/assets/yml/form/hablame.status.form.yml'));
    foreach ($ymlFormFields as $key => $field) {
      $form[$key] = $field;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $smsId = $form_state->getValue('smsid');

    if (!empty($smsId)) {
      $result = $this->hablame->getStatus($smsId);
      if ($result !== FALSE) {
        $this->hablameHelper->setDetails($result);
        $this->messenger()->addMessage($this->t('Status consulted successfully'));
        $form_state->setRedirect('hablame.sms.collection');
      }
      else {
        $this->messenger()->addMessage($this->t('Error while checking status see the log site for more information.'));
      }
    }
  }

}
