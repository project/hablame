<?php

namespace Drupal\hablame\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameHelper;
use Drupal\hablame\Service\HablameService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The test form for the sms services.
 */
class SmsTestForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Variable that store the hablame Helper.
   *
   * @var \Drupal\hablame\Service\HablameHelper
   */
  protected $hablameHelper;

  /**
   * Variable that store the module handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\hablame\Service\HablameHelper $hablameHelper
   *   The hablame helper.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    HablameService $hablame,
    HablameHelper $hablameHelper
    ) {
    $this->moduleHandler = $moduleHandler;
    $this->hablame = $hablame;
    $this->hablameHelper = $hablameHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('hablame.service'),
      $container->get('hablame.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_sms_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $module_path = $this->moduleHandler->getModule('hablame')->getPath();

    $ymlFormFields = Yaml::decode(file_get_contents($module_path . '/assets/yml/form/hablame.test.form.yml'));
    foreach ($ymlFormFields as $key => $field) {
      $form[$key] = $field;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $type = $form_state->getValue('type_of_sms');
    $phone = $form_state->getValue('phone');
    $message = $form_state->getValue('message');
    $flash = $form_state->getValue('flash');
    $certified = $form_state->getValue('certified');
    $result = FALSE;

    if ($type == 'marketing') {
      if (!empty($message) && !empty($phone)) {
        if ($flash == '1') {
          if ($certified == '1') {
            $result = $this->hablame->sendMarketingMessage($phone, $message, TRUE, TRUE);
          }
          else {
            $result = $this->hablame->sendMarketingMessage($phone, $message, TRUE);
          }
        }
        elseif ($certified == '1') {
          if ($flash == '1') {
            $result = $this->hablame->sendMarketingMessage($phone, $message, TRUE, TRUE);
          }
          else {
            $result = $this->hablame->sendMarketingMessage($phone, $message, FALSE, TRUE);
          }
        }
        else {
          $result = $this->hablame->sendMarketingMessage($phone, $message);
        }
        if ($result !== FALSE) {
          $this->hablameHelper->createSmsEntity($result);
          $this->messenger()->addMessage($this->t(
            'Marketing message successfully sended.
            The id of your message to consult the status is @result',
          [
            '@result' => print_r($result['smsId'], TRUE),
          ]));
        }
        else {
          $this->messenger()->addMessage($this->t('Error sending the message see the site log for more information.'));
        }
      }
    }
    else {
      if (!empty($message) && !empty($phone)) {
        if ($flash == '1') {
          if ($certified == '1') {
            $result = $this->hablame->sendPriorityMessage($phone, $message, TRUE, TRUE);
          }
          else {
            $result = $this->hablame->sendPriorityMessage($phone, $message, TRUE);
          }
        }
        elseif ($certified == '1') {
          if ($flash == '1') {
            $result = $this->hablame->sendPriorityMessage($phone, $message, TRUE, TRUE);
          }
          else {
            $result = $this->hablame->sendPriorityMessage($phone, $message, FALSE, TRUE);
          }
        }
        else {
          $result = $this->hablame->sendPriorityMessage($phone, $message);
        }
        if ($result !== FALSE) {
          $this->hablameHelper->createSmsEntity($result);
          $this->messenger()->addMessage($this->t(
            'Priority message successfully sended.
            The id of your message to consult the status is @result',
          [
            '@result' => print_r($result['smsId'], TRUE),
          ]));
        }
        else {
          $this->messenger()->addMessage($this->t('Error sending the message see the site log for more information.'));
        }
      }
    }

  }

}
