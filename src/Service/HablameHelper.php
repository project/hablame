<?php

namespace Drupal\hablame\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a repository for Block config entities.
 */
class HablameHelper {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * Variable that store the HTTP client.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $client;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Var that store the logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Hablame constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The file storage backend.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Http\ClientFactory $client
   *   Var that stores the HTTP client.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    ConfigFactory $config,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    ClientFactory $client,
    HablameService $hablame,
    LoggerChannelFactoryInterface $logger,
    MessengerInterface $messenger
  ) {
    $this->config = $config;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->settings = $config->get('hablame.settings');
    $this->client = $client;
    $this->hablame = $hablame;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('http_client_factory'),
      $container->get('hablame.service'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * Function to create a sms.
   */
  public function createSmsEntity($data) {

    if ($data !== FALSE) {

      if ($data['status'] == '1x000') {
        $status = $this->t('sended');
      }
      else {
        $status = $this->t('not sended');
      }
      /** @var \Drupal\hablame\Entity\Sms $smsEntity */
      $smsEntity = $this->entityTypeManager->getStorage('sms')->create([
        'smsId' => $data['smsId'],
        'status' => $status,
        'details' => $this->t('Unrevised'),
      ]);

      $smsEntity->save();
      return $smsEntity;
    }
    else {
      $this->logger->get('smsEntity')->error($this->t('The sms entity could not be created.'));
    }
  }

  /**
   * Function to set detils value.
   */
  public function setDetails($data) {

    if ($data !== FALSE) {
      if ($data['status'] == '1x151') {
        $status = $this->t('sended')->render();
      }
      else {
        $status = $this->t('not sended')->render();
      }
      $details = [
        $this->t('status')->render() => $status,
        $this->t('error')->render() => $data['error'],
        $this->t('error_description')->render()
        => $this->getErrorDescription($data),
        $this->t('price')->render() => $data['price'],
        $this->t('loteId')->render() => $data['loteId'],
        $this->t('smsId')->render() => $data['smsId'],
        $this->t('sms')->render() => $data['sms'],
        $this->t('sc')->render() => $data['sc'],
        $this->t('flash')->render() =>
        $this->isCertifiedAndFlash($data['flash']),
        $this->t('Is Priority')->render() =>
        $this->isCertifiedAndFlash($data['isPriority']),
        $this->t('Is Certified')->render() =>
        $this->isCertifiedAndFlash($data['request_dlvr_rcpt']),
        $this->t('Send method')->render() => $this->getSendMethod($data),
        $this->t('Numero')->render() => $data['numero'],
        $this->t('Type')->render() => $this->getType($data),
        $this->t('ip')->render() => $data['ip'],
        $this->t('Billing parts')->render() => $data['billingParts'],
        $this->t('Date of request for SMS delivery')->render() =>
        date("Y-m-d H:i:s", $data['ecare_time_step01']),
        $this->t('Date of SMS cancellation')->render() =>
        ($data['ecare_time_step02'] == 0) ? '' :
        date("Y-m-d H:i:s", $data['ecare_time_step02']),
        $this->t('Date of SMS scheduling')->render() =>
        date("Y-m-d H:i:s", $data['ecare_time_step03']),
        $this->t('Date of notification of delivery to the cell phone')
          ->render() => ($data['ecare_time_step04'] == 0) ? '' :
        date("Y-m-d H:i:s", $data['ecare_time_step04']),
        $this->t('Date')->render() =>
        ($data['ecare_time_step05'] == 0) ? '' :
        date("Y-m-d H:i:s", $data['ecare_time_step05']),
        $this->t('Billing Characters')->render() =>
        $data['billingCharacters'],
        $this->t('signature')->render() =>
        is_null($data['signature']) ? '' : $data['signature'],
      ];
      $entities = $this->entityTypeManager
        ->getStorage('sms')
        ->loadByProperties([
          'smsId' => $data['smsId'],
        ]);

      foreach ($entities as $smsEntity) {
        /** @var \Drupal\hablame\Entity\Sms $smsEntity */
        $output = implode("\n", array_map(function ($v, $k) {
          return sprintf("%s='%s'", $k, $v);
        }, $details, array_keys($details)
        ));
        $smsEntity->set('details', $output);
        $smsEntity->save();
      }

    }
    else {
      $this->logger->get('smsEntity')->error($this->t('The sms entity details not be updated.'));
    }

  }

  /**
   * Function to get the error description.
   */
  public function getErrorDescription($data) {
    $errors = [
      '1x005' => $this->t('empty origin IP.')->render(),
      '1x006' => $this->t('Invalid shipping date.')->render(),
      '1x007' => $this->t('Shipping date exceeds 2 months.')->render(),
      '1x009' => $this->t(
        'No messages to send, make sure to send the toNumber and sms fields.'
      )->render(),
      '1x010' => $this->t('Too many numbers in the toNumber field.')->render(),
      '1x011' => $this->t('Account has a general block.')->render(),
      '1x012' => $this->t('Account is blocked due to fraud.')->render(),
      '1x013' => $this->t('Account is blocked by portfolio.')->render(),
      '1x014' => $this->t(
        'Client does not have the SMS module enabled.')->render(),
      '1x015' => $this->t(
        'Client does not have the use of the API enabled.'
      )->render(),
      '1x016' => $this->t(
        'Internal error - no account exists in dbr.'
      )->render(),
      '1x017' => $this->t(
        'A priority SMS cannot have more than one destination number.'
      )->render(),
      '1x018' => $this->t(
        'You have exceeded the limit of sends per second for a priority SMS.'
      )->render(),
      '1x019' => $this->t('IP not enabled to send SMS.')->render(),
      '1x020' => $this->t(
        'You do not have a balance available to make the shipment.'
      )->render(),
      '1x021' => $this->t('Priority SMS cannot be programmed.')->render(),
    ];

    if (array_key_exists($data['error'], $errors)) {
      return $errors[$data['error']];
    }
    else {
      return '';
    }
  }

  /**
   * Function to get the error description.
   */
  public function getSendMethod($data) {
    $sendedMethods = [
      '1' => $this->t('SMS sent from API.')->render(),
      '2' => $this->t('SMS sent from ecare 1 to 1.')->render(),
      '3' => $this->t('SMS enviado desde ecare file.')->render(),
      '4' => $this->t('SMS enviado desde SMPP.')->render(),
    ];

    if (array_key_exists(strval($data['method_send']), $sendedMethods)) {
      return $sendedMethods[strval($data['method_send'])];
    }
    else {
      return '';
    }
  }

  /**
   * Function to get the type of message.
   */
  public function getType($data) {
    $messagesTypes = [
      '0' => $this->t('SMS standard.')->render(),
      '1' => $this->t('SMS premium.')->render(),
    ];

    if (array_key_exists(strval($data['type']), $messagesTypes)) {
      return $messagesTypes[strval($data['type'])];
    }
    else {
      return '';
    }
  }

  /**
   * Function to identify if is certified and flash.
   */
  public function isCertifiedAndFlash($data) {
    if (strval($data) == '1') {
      return $this->t('yes')->render();
    }
    else {
      return $this->t('no')->render();
    }
  }

  /**
   * Finish batch.
   *
   * @param bool $success
   *   Indicates whether the batch process was successful.
   * @param array $results
   *   Results information passed from the processing callback.
   */
  public function finish($success, array $results) {
    if (!empty($results)) {
      if ($success) {
        $this->messenger->addMessage($this->t('Items - @count were processed.', [
          '@count' => $results['count'],
        ]));

        $this->messenger->addMessage($this->t('Items - @count were created.', [
          '@count' => $results['sucess'],
        ]));

        if (is_null($results['error'])) {
          $this->messenger->addError($this->t('Any entities found'));
        }
        else {
          if (count($results['error']) > 0) {
            $this->messenger->addError($this->t('Items were not imported to the following states: @error.', [
              '@error' => implode(", ", $results['error']),
            ]));
          }
        }
      }
      else {
        $this->messenger->addError($this->t('An error occurred trying to import State Sectors.'));
      }
    }
    else {
      $this->messenger->addError($this->t('Any entities found'));
    }
  }

  /**
   * Processes the Bynder resources.
   *
   * @param mixed $eid
   *   The order ids.
   * @param string $entityType
   *   The type of entity.
   * @param array|\ArrayAccess $context
   *   The batch context.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures an exception is thrown.
   */
  public function processEntityGroup($eid, $entityType, &$context) {

    if (empty($context['results'])) {
      $context['results']['sucess'] = 0;
      $context['results']['count'] = 0;
      $context['results']['error'] = [];
    }

    if ($entityType === 'sms') {
      /** @var \Drupal\hablame\Entity\Sms $entity */
      $entity = $this->entityTypeManager->getStorage('sms')->load($eid);
    }
    else {
      /** @var \Drupal\hablame\Entity\UrlShort $entity */
      $entity = $this->entityTypeManager->getStorage('url_short')->load($eid);
    }

    if (is_object($entity)) {
      $entity->delete();
      $context['results']['sucess']++;
    }
    else {
      $context['results']['error'][] = $eid;
    }

    $context['results']['count']++;
  }


  /**
   * Function to create a url.
   */
  public function createUrlEntity($data) {

    if ($data !== FALSE) {

      /** @var \Drupal\hablame\Entity\UrlShort $urlEntity */
      $urlEntity = $this->entityTypeManager->getStorage('url_short')->create([
        'token' => $data['data']['token'],
        'short_url' => $data['data']['short_url'],
        'url' => $data['data']['url'],
      ]);

      $urlEntity->save();
      return $urlEntity;
    }
    else {
      $this->logger->get('urlEntity')->error($this->t('The url entity could not be created.'));
    }
  }

}
