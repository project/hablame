<?php

namespace Drupal\hablame\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a repository for Block config entities.
 */
class HablameService {

  const AUTH_PATH_PROD  = 'https://api103.hablame.co/api/';
  const AUTH_PATH_STAGE = 'https://stoplight.io/mocks/hablame-colombia/api-sms/10725406/';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * Variable that store the HTTP client.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $client;

  /**
   * Var that store the logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * B2cHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Http\ClientFactory $client
   *   Var that stores the HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   */
  public function __construct(
    ConfigFactory $config,
    FileSystemInterface $fileSystem,
    ClientFactory $client,
    LoggerChannelFactoryInterface $logger
  ) {
    $this->config = $config;
    $this->fileSystem = $fileSystem;
    $this->settings = $config->get('hablame.settings');
    $this->client = $client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('http_client_factory'),
      $container->get('logger.factory')
    );
  }

  /**
   * Function to get service strings.
   */
  private function getString(string $key) {
    $test = boolval($this->settings->get('test'));
    $string = '';
    switch ($key) {
      case 'auth_path':
        $string = ($test === TRUE) ? self::AUTH_PATH_STAGE : self::AUTH_PATH_PROD;
        break;
    }
    return $string;
  }

  /**
   * Function to transorm a url into a short url.
   *
   * @param string $url
   *   The url to convert in a short url.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function getShortUrl(string $url) {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    $data = [
      'url' => $url,
    ];

    try {
      $response = $client->post('url-shortener/v1/token', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
        'json' => $data,
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;

  }

  /**
   * Function to send a priority message.
   *
   * @param string $phone
   *   The phone number to send message.
   * @param string $message
   *   The message to send.
   * @param bool $flash
   *   An option for define if the message is flash or not.
   * @param bool $certified
   *   An option for define if the message is certified.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function sendPriorityMessage(string $phone, string $message, bool $flash = FALSE, bool $certified = FALSE) {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    $data = [
      'toNumber' => $phone,
      'sms' => $message,
      'flash' => intval($flash),
      'request_dlvr_rcpt' => intval($certified),
    ];

    try {
      $response = $client->post('sms/v3/send/priority', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
        'json' => $data,
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame_priority_message')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;

  }

  /**
   * Function to send a marketing message.
   *
   * @param string $phone
   *   The phone number to send message.
   * @param string $message
   *   The message to send.
   * @param bool $flash
   *   An option for define if the message is flash or not.
   * @param bool $certified
   *   An option for define if the message is certified.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function sendMarketingMessage(string $phone, string $message, bool $flash = FALSE, bool $certified = FALSE) {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    $data = [
      'toNumber' => $phone,
      'sms' => $message,
      'flash' => intval($flash),
      'request_dlvr_rcpt' => intval($certified),
    ];

    try {
      $response = $client->post('sms/v3/send/marketing', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
        'json' => $data,
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame_marketing_message')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to send masive marketing messages.
   *
   * @param array $content
   *   An array of objects with the number and message.
   * @param bool $flash
   *   An option for define if the message is flash or not.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function sendMarketingBulk(array $content, bool $flash = FALSE) {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    $data = [
      'flash' => intval($flash),
      'bulk' => [
        $content,
      ],
    ];

    try {
      $response = $client->post('sms/v3/send/marketing/bulk', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
        'json' => $data,
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame_marketing_message')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to upload a file to the hablame cloud.
   *
   * @param string $absoluteFilePath
   *   The absolute path of the upload file.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function uploadFile(string $absoluteFilePath) {
    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $response = $client->post('callblasting/v1/callblasting/audio_load', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
        'multipart' => [
          [
            'name' => 'audio',
            'contents' => Utils::tryFopen($absoluteFilePath, 'r'),
          ],
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to get a message status.
   *
   * @param string $smsId
   *   The message id.
   *
   * @return mixed
   *   The status of the message or False if no response.
   */
  public function getStatus($smsId) {
    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $response = $client->get('sms/v3/report/' . $smsId, [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to get a message status.
   *
   * @return mixed
   *   The status of the hablame account or False if no response.
   */
  public function getAccountStatus() {
    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $response = $client->get('account/v1/status', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settings->get('account'),
          'apikey' => $this->settings->get('api_key'),
          'token' => $this->settings->get('token'),
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

}
