<?php

namespace Drupal\hablame\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\hablame\SmsInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the sms entity class.
 *
 * @ContentEntityType(
 *   id = "sms",
 *   label = @Translation("Sms"),
 *   label_collection = @Translation("Smss"),
 *   label_singular = @Translation("sms"),
 *   label_plural = @Translation("smss"),
 *   label_count = @PluralTranslation(
 *     singular = "@count smss",
 *     plural = "@count smss",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\hablame\SmsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\hablame\Form\SmsForm",
 *       "add" = "Drupal\hablame\Form\SmsForm",
 *       "edit" = "Drupal\hablame\Form\SmsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "sms",
 *   data_table = "sms_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sms",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sms",
 *     "add-form" = "/sms/add",
 *     "canonical" = "/sms/{sms}",
 *     "edit-form" = "/sms/{sms}/edit",
 *     "delete-form" = "/sms/{sms}/delete",
 *   },
 *   field_ui_base_route = "entity.sms.settings",
 * )
 */
class Sms extends ContentEntityBase implements SmsInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setDescription(new TranslatableMarkup('The ID of the sms entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(new TranslatableMarkup('UUID'))
      ->setDescription(new TranslatableMarkup('The UUID of the sms entity.'))
      ->setReadOnly(TRUE);

    $fields['smsId'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('smsId'))
      ->setDescription(new TranslatableMarkup('The sms id.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setDescription(new TranslatableMarkup('The status of the message.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => 'send',
        'max_length' => 40,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setReadOnly(TRUE);

    $fields['details'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Details'))
      ->setDescription(new TranslatableMarkup('The extra information of the sms.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'textarea',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('User Name'))
      ->setDescription(new TranslatableMarkup('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(new TranslatableMarkup('Language code'))
      ->setDescription(new TranslatableMarkup('The language code of Contract entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    return $fields;
  }

}
