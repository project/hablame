<?php

namespace Drupal\hablame\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameHelper;
use Drupal\hablame\Service\HablameService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SmsStatusController.
 *
 * @package Drupal\hablame\Controller
 */
class SmsStatusController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Variable that store the hablame Helper.
   *
   * @var \Drupal\hablame\Service\HablameHelper
   */
  protected $hablameHelper;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Var that store the logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Hablame constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The file storage backend.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\hablame\Service\HablameHelper $hablameHelper
   *   The hablame helper.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    HablameService $hablame,
    HablameHelper $hablameHelper,
    LoggerChannelFactoryInterface $logger
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->hablame = $hablame;
    $this->hablameHelper = $hablameHelper;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('hablame.service'),
      $container->get('hablame.helper'),
      $container->get('logger.factory')
    );
  }

  /**
   * Function that updates the smsStatus.
   *
   * @param string $smsId
   *   The id from the sms entity.
   */
  public function call(string $smsId) {
    /** @var \Drupal\hablame\Entity\Sms $smsEntity */
    $smsEntity = $this->entityTypeManager->getStorage('sms')->load($smsId);
    if ($smsEntity->get('details')->value !== 'Unrevised') {
      $this->messenger()->addMessage($this->t('The status of the sms has already been consulted.'));
    }
    else {
      $result = $this->hablame->getStatus($smsEntity->get('smsId')->value);
      if ($result !== FALSE) {
        $this->hablameHelper->setDetails($result);
        $this->messenger()->addMessage($this->t('Status consulted successfully'));
        return $this->redirect('hablame.sms.collection');
      }
      else {
        $this->messenger()->addMessage($this->t('Error while checking status see the log site for more information.'));
      }
    }
  }

}
