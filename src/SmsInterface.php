<?php

namespace Drupal\hablame;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a sms entity type.
 */
interface SmsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
