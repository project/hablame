<?php

namespace Drupal\hablame\Command;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameService;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A drush command file.
 *
 * @package Drupal\hablame\Command
 */
class HablameCommand extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Var that store the hablame service.
   *
   * @var Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * {@inheritdoc}
   */
  public function __construct(HablameService $hablame) {
    $this->hablame = $hablame;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
      $container->get('hablame.service')
    );
  }

  /**
   * Drush command that get short url.
   *
   * @command hablame:shortUrl
   * @option url
   * The url to be shortened.
   * @aliases hsu
   * @usage hablame:shortUrl --url
   */
  public function shortUrl($options = [
    'url' => '',
  ]) {
    print_r($this->hablame->getShortUrl($options['url']));
  }

  /**
   * Drush command to send a priority message.
   *
   * @command hablame:prioritymessage
   * @option phone
   * The phone number to send message.
   * @option message
   * The message to send.
   * @option flash
   * An option for define if the message is flash or not.
   * @option certified
   * An option for define if the message is certified or not.
   * @aliases hspm
   * @usage hablame:prioritymessage --phone --message --flash --certified
   * Examples:
   *
   * hablame:prioritymessage --phone='57316xxxxxxx'
   * --message='hi this is a test priority message' --flash=FALSE
   */
  public function commandSendPriorityMessage($options = [
    'phone' => '',
    'message' => '',
    'flash' => '',
    'certified' => '',
  ]) {
    print_r($this->hablame->sendPriorityMessage($options['phone'], $options['message'], $options['flash'], $options['certified']));
  }

  /**
   * Drush command to send a marketing message.
   *
   * @command hablame:marketingmessage
   * @option phone
   * The phone number to send message.
   * @option message
   * The message to send.
   * @option flash
   * An option for define if the message is flash or not.
   * @aliases hsmm
   * @usage hablame:marketingmessage --phone --message --flash
   *  Examples:
   *
   * hablame:prioritymessage --phone='57316xxxxxxx'
   * --message='hi this is a test marketing message'
   * --flash=FALSE
   * --certified=FALSE
   */
  public function commandSendMarketingMessage($options = [
    'phone' => '',
    'message' => '',
    'flash' => '',
    'certified' => '',
  ]) {
    print_r($this->hablame->sendMarketingMessage($options['phone'], $options['message'], $options['flash'], $options['certified']));
  }

  /**
   * Drush command to send multiple marketing messages.
   *
   * @command hablame:marketingbulk
   * @option content
   * An array of objects with the number and message.
   * @option flash
   * An option for define if the message is flash or not.
   * @aliases hsbm
   * @usage hablame:marketingbulk --content --flash
   *  Examples:
   *
   * hablame:prioritymessage --content=[
   * {'numero': '57316xxxxxxx', 'sms': 'hi'},
   * {'numero': '57315xxxxxxx', 'sms': 'hi again'}
   * ]
   * --flash=FALSE
   */
  public function commandSendMarketingBulk($options = [
    'content' => [],
    'flash' => '',
  ]) {
    print_r($this->hablame->sendMarketingMessage($options['phone'], $options['message'], $options['flash']));
  }

  /**
   * Drush command to get the account status.
   *
   * @command hablame:accountStatus
   * @aliases has
   * @usage hablame:accountStatus
   */
  public function commandGetAccountStatus() {
    print_r($this->hablame->getAccountStatus());
  }

  /**
   * Drush command that upload a file in the hablame cloud.
   *
   * @command hablame:uploadFile
   * @option url
   * The url of the file.
   * @aliases huf
   * @usage hablame:uploadFile --url
   */
  public function uploadFile($options = [
    'url' => '',
  ]) {
    print_r($this->hablame->uploadFile($options['url']));
  }

  /**
   * Drush command that upload a file in the hablame cloud.
   *
   * @command hablame:smsStatus
   * @option smsId
   * The message id.
   * @aliases hsmss
   * @usage hablame:smsStatus --smsId
   */
  public function smsStatus($options = [
    'smsId' => '',
  ]) {
    print_r($this->hablame->getStatus($options['smsId']));
  }

}
