<?php

namespace Drupal\hablame_audio\Command;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;
use Drupal\hablame_audio\Service\HablameAudioService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A drush command file.
 *
 * @package Drupal\hablame_audio\Command
 */
class HablameAudioCommand extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Var that store the hablame audio service.
   *
   * @var Drupal\hablame_audio\Service\HablameAudioService
   */
  protected $hablameAudio;

  /**
   * {@inheritdoc}
   */
  public function __construct(HablameAudioService $hablameAudio) {
    $this->hablameAudio = $hablameAudio;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
      $container->get('hablame_audio.service')
    );
  }

  /**
   * Drush command that ceate an own audio file by given text.
   *
   * @command hablame_audio:otp
   * @option code
   * Argument with code to be created.
   * @aliases haotp
   * @usage hablame_audio:otp --code
   * Examples:
   *
   * hablame_audio:otp --code="1966"
   */
  public function otp($options = [
    'code' => '',
  ]) {
    print_r($this->hablameAudio->otp($options['code']));
  }

  /**
   * Drush command that send a otp with upload file.
   *
   * @command hablame_audio:sendOtp
   * @option code
   * Argument with code to be created as otp.
   * @option number
   * The phone number to send the otp.
   * @aliases hasotp
   * @usage hablame_audio:sendOtp --code --number
   * Examples:
   *
   * hablame_audio:otp --code="1966" --phone="573165676291"
   */
  public function sendOtp($options = [
    'code' => '',
    'number' => '',
  ]) {
    print_r($this->hablameAudio->audioOtp($options['code'], $options['number']));
  }

  /**
   * Drush command that get the list of available voices for audio services.
   *
   * @command hablame_audio:listVoices
   * @aliases halv
   * @usage hablame_audio:listVoices
   */
  public function hablameAvailableVoices() {
    print_r($this->hablameAudio->textToSpeechListVocies());
  }

  /**
   * Drush command to test the text to speech service.
   *
   * @command hablame_audio:textSpeech
   * @option text
   * The text to transform to voice.
   * @option voice
   * The voice selected form the hablame options.
   * @aliases hatts
   * @usage hablame_audio:textSpeech --text --voice
   * Examples:
   *
   * hablame_audio:otp --code="Hi this is a test" --voice="ue-en"
   */
  public function textToSpeech($options = [
    'code' => '',
    'number' => '',
  ]) {
    print_r($this->hablameAudio->audioOtp($options['code'], $options['number']));
  }

}
