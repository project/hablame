<?php

namespace Drupal\hablame_audio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines hablame audio form configuration.
 */
class HablameAudioSettingsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hablame_audio.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_audio_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hablame_audio.settings');
    $form['target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target folder'),
      '#description' => $this->t('Where the files will be saved publicly.'),
      '#default_value' => $config->get('target'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('hablame_audio.settings')
      // ->set('binary', $form_state->getValue('binary'))
      ->set('target', $form_state->getValue('target'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
