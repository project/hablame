<?php

namespace Drupal\hablame_audio\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hablame\Service\HablameService;
use Drupal\hablame_audio\Service\HablameAudioService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test form for the audio message with Hablame.
 */
class TestForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame_audio\Service\HablameAudioService
   */
  protected $hablameAudio;

  /**
   * Variable that store the module handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param \Drupal\hablame_audio\Service\HablameAudioService $hablameAudio
   *   The Hablame Audio Service.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    HablameService $hablame,
    HablameAudioService $hablameAudio
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->hablame = $hablame;
    $this->hablameAudio = $hablameAudio;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('hablame.service'),
      $container->get('hablame_audio.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hablame_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $module_path = $this->moduleHandler->getModule('hablame_audio')->getPath();

    $ymlFormFields = Yaml::decode(file_get_contents($module_path . '/assets/yml/form/hablame_audio.test.form.yml'));
    foreach ($ymlFormFields as $key => $field) {
      $form[$key] = $field;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $phone = $form_state->getValue('phone');
    $message = $form_state->getValue('message');
    $queue = $form_state->getValue('queue');
    $otp = $form_state->getValue('otp');

    if (!empty($phone)) {
      if (!empty($message)) {
        if ($queue) {
          $this->messenger()->addMessage($this->t('Message saved in sending queue.'));
          $queue = \Drupal::queue('hablame_send_message_queue');
          $queue->createQueue();
          $queue->createItem([
            'phone' => $phone,
            'message' => $message,
          ]);
        }
        else {
          $this->messenger()->addMessage($this->t('Message sent successfully.'));
          $this->hablame->sendPriorityMessage($phone, $message);
        }
      }

      if (!empty($otp)) {
        if ($queue) {
          $queue = \Drupal::queue('hablame_send_audio_otp_queue');
          $queue->createQueue();
          $queue->createItem([
            'phone' => $phone,
            'otp' => $otp,
          ]);
        }
        else {
          $this->hablameAudio->audioOtp($otp, $phone);
        }

      }
    }
  }

}
