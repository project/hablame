<?php

namespace Drupal\hablame_audio\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\hablame\Service\HablameService;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides a repository for Block config entities.
 */
class HablameAudioService {

  const AUTH_PATH_PROD = 'https://api103.hablame.co/api/';
  const AUTH_PATH_STAGE = 'http://localhost:10001/api/url-shortener/v1';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Variable that store the HTTP client.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $client;

  /**
   * The module hanbdler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settingsHablame;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $phpFfmpegSettings;

  /**
   * Variable that store the Hablame Service.
   *
   * @var \Drupal\hablame\Service\HablameService
   */
  protected $hablame;

  /**
   * The lock.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * B2cHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\Http\ClientFactory $client
   *   Var that stores the HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\hablame\Service\HablameService $hablame
   *   The Hablame Service.
   * @param Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   */
  public function __construct(
    ConfigFactory $config,
    ClientFactory $client,
    ModuleHandlerInterface $moduleHandler,
    FileSystemInterface $fileSystem,
    HablameService $hablame,
    LockBackendInterface $lock,
    LoggerChannelFactoryInterface $logger
  ) {
    $this->config = $config;
    $this->client = $client;
    $this->moduleHandler = $moduleHandler;
    $this->fileSystem = $fileSystem;
    $this->hablame = $hablame;
    $this->settingsHablame = $config->get('hablame.settings');
    $this->phpFfmpegSettings = $config->get('php_ffmpeg_settings');
    $this->lock = $lock;
    $this->logger = $logger;
  }

  /**
   * Function to get service strings.
   */
  private function getString(string $key) {
    $test = boolval($this->settingsHablame->get('test'));
    $string = '';
    switch ($key) {
      case 'auth_path':
        $string = ($test === TRUE) ? self::AUTH_PATH_STAGE : self::AUTH_PATH_PROD;
        break;
    }
    return $string;
  }

  /**
   * Function to create an own OTP as mp3 file.
   *
   * @param string $code
   *   The opt message.
   *
   * @return string
   *   The file name of the otp.
   */
  public function otp(string $code) {

    $rawNumbers = str_split($code);
    $numbers = [];
    foreach ($rawNumbers as $number) {
      if (strpos('0123456789', $number) !== FALSE) {
        $numbers[] = $number;
      }
    }

    $code = 's' . implode($numbers) . 'r' . implode($numbers);
    $chars = str_split($code);
    $filename = '';

    $sources = [
      '0' => '0.mp3',
      '1' => '1.mp3',
      '2' => '2.mp3',
      '3' => '3.mp3',
      '4' => '4.mp3',
      '5' => '5.mp3',
      '6' => '6.mp3',
      '7' => '7.mp3',
      '8' => '8.mp3',
      '9' => '9.mp3',
      's' => 'saludo.mp3',
      'r' => 'te-lo-repito.mp3',
    ];

    $files = [];
    $basePath = $this->moduleHandler->getModule('hablame_audio')->getPath() . '/assets/audio/';
    foreach ($chars as $char) {
      if (isset($sources[$char])) {
        $files[] = $basePath . $sources[$char];
        $filename .= $char;
      }
    }

    $target = rtrim($this->settingsHablame->get('target'), '/');
    $rawTargetParts = explode('/', $target);
    $targetParts = [];

    foreach ($rawTargetParts as $part) {
      if ($part !== '/') {
        $targetParts[] = $part;
        $folder = $this->fileSystem->realpath('public://' . implode('/', $targetParts));
        $this->fileSystem->prepareDirectory(
          $folder,
          $this->fileSystem::CREATE_DIRECTORY | $this->fileSystem::MODIFY_PERMISSIONS
        );
      }
    }

    $filename = $this->fileSystem->realpath('public://' . implode('/', $targetParts)) . '/' . $filename . '.mp3';

    if (!file_exists($filename)) {
      $this->concat($files, $filename);
    }

    return $filename;
  }

  /**
   * Function to send audio OTP from audio_id.
   *
   * @param string $code
   *   The opt code.
   * @param string $phone
   *   The phone number to send the otp message.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function audioOtp(string $code, string $phone) {

    $filename = $this->otp($code);
    $upload = $this->hablame->uploadFile($filename);
    if (!isset($upload['audio_id'])) {
      return FALSE;
    }

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $this->logger->get('hablame')->info(print_r($upload['audio_id'], TRUE));
      $response = $client->post('callblasting/v1/callblasting/audio_id', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settingsHablame->get('account'),
          'apikey' => $this->settingsHablame->get('api_key'),
          'token' => $this->settingsHablame->get('token'),
        ],
        'json' => [
          "toNumber" => $phone,
          "audio_id" => $upload['audio_id'],
          "sendDate" => '',
          "attempts" => '2',
          "attempts_delay" => '30',
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;

  }

  /**
   * Function to send audio OTP from audio_id.
   *
   * @param string $text
   *   The text to transfomr to voice.
   * @param string $voice
   *   The voice selected form the hablame options.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function textToSpeech(string $text, string $voice) {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $response = $client->post('text-to-speech/v1/text', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settingsHablame->get('account'),
          'apikey' => $this->settingsHablame->get('api_key'),
          'token' => $this->settingsHablame->get('token'),
        ],
        'json' => [
          "text" => $text,
          "voice_name" => $voice,
          "setPitch" => '0',
          "setSpeakingRate" => '1',
          "setSampleRateHertz" => '24000',
          "setVolumeGainDb" => '0',
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame_audio_text_to_speech_response')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame_audio')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to get the list of voices available.
   *
   * @return mixed
   *   The response to the petition or False if an error exist.
   */
  public function textToSpeechListVocies() {

    /** @var \GuzzleHttp\Client $client */
    $client = $this->client->fromOptions([
      'base_uri' => $this->getString('auth_path'),
    ]);

    try {
      $response = $client->get('api/text-to-speech/v1/voices', [
        'headers' => [
          'Accept' => 'application/json',
          'account' => $this->settingsHablame->get('account'),
          'apikey' => $this->settingsHablame->get('api_key'),
          'token' => $this->settingsHablame->get('token'),
        ],
      ]);

      $result = Json::decode($response->getBody());
      $this->logger->get('hablame_list_of_voices')->info(print_r($result, TRUE));
      return $result;
    }
    catch (ClientException $exception) {
      $this->logger->get('hablame_audio')->error($exception->getMessage());
    }
    catch (RequestException $exception) {
      $this->logger->get('hablame')->error($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * Function to create concat.
   */
  public function concat($files = [], $target = NULL) {
    $binary = $this->phpFfmpegSettings->get('binary');

    $resources = '';
    // -i /app/web/modules/custom/ffmpeg/assets/audio/1.mp3
    $filters = " -filter_complex '";
    // -filter_complex '[0:0][1:0][2:0]concat=n=3:v=0:a=1[out]' -map '[out]'
    $i = 0;
    foreach ($files as $file) {
      $resources .= ' -i ' . $file;
      $filters .= "[{$i}:0]";
      $i++;
    }
    $filters .= "concat=n={$i}:v=0:a=1[out]' -map '[out]' ";

    $command = $binary . $resources . $filters . $target;

    $lock = $this->lock;
    if ($lock->acquire(__FILE__) !== FALSE) {
      shell_exec($command);
      $lock->release(__FILE__);
    }
    else {
      while ($lock->acquire(__FILE__) === FALSE) {
        $lock->wait(__FILE__, 3);
      }
      if ($lock->acquire(__FILE__) !== FALSE) {
        shell_exec($command);
        $lock->release(__FILE__);
      }
    }
  }

}
