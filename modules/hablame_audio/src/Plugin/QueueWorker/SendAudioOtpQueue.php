<?php

namespace Drupal\hablame_audio\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\hablame_audio\Service\HablameAudioService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "hablame_send_audio_otp_queue",
 *   title = @Translation("Hablame worker: Audio OTP callblasting"),
 *   cron = {"time" = 30}
 * )
 */
class SendAudioOtpQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Var that store the hablame service.
   *
   * @var \Drupal\hablame_audio\Service\HablameAudioService
   */
  protected $hablameAudio;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HablameAudioService $hablameAudio, LoggerChannelFactoryInterface $loggerInterface) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->hablameAudio = $hablameAudio;
    $this->logger = $loggerInterface->get('hablame');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hablame_audio.service'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (isset($data['phone']) && isset($data['otp'])) {
      $this->hablameAudio->audioOtp($data['otp'], $data['phone']);
    }
    else {
      $this->logger->get('hablame_audio')->notice('Hablame - callblasting not send: ' . print_r($data, TRUE));
    }

  }

}
